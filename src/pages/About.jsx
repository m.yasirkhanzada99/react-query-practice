import {Link} from "react-router-dom"
export const About = () => {
    return (
        <div className="p-[22px]">
            <img src="https://scrimba.com/blobs/sha1:62dc25385aae8a021bdb76ca3cc6b034b687b81b.png" className="about-hero-image" />
            <div className="about-page-content">
                <h1 className="mt-[2rem] mb-[1rem] text-2xl font-extrabold">Don’t squeeze in a sedan when you could relax in a van.</h1>
                <p className='font-medium text-gray-500 mt-[2rem]'>Our mission is to enliven your road trip with the perfect travel van rental. Our vans are recertified before each trip to ensure your travel plans can go off without a hitch. (Hitch costs extra 😉)</p>
                <p className="font-medium text-gray-500 mt-[1rem]">Our team is full of vanlife enthusiasts who know firsthand the magic of touring the world on 4 wheels.</p>
            </div>
            <div className="mt-[2rem] p-[20px] rounded-2xl bg-amber-200">
                <h2 className="font-bold text-2xl mb-[2rem] ">Your destination is waiting.<br />Your van is ready.</h2>
                <Link className="text-2xl rounded-xl px-2 py-2 text-white bg-black" to="/vans">Explore our vans</Link>
            </div>
        </div>
    )
}