import {Link} from "react-router-dom";

export const Navbar = () => {
    return (
        <div className="p-4 mx-5 flex justify-between items-center ">
           <Link className="text-4xl font-extrabold" to="/">#Tripper</Link>
            <nav className="flex gap-6 text-2xl">
                <Link  to="/home">Home</Link>
                <Link to="/about">About</Link>
                <Link to="/vans">Vans</Link>
            </nav>
        </div>
    )
}