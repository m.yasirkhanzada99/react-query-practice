import './App.css'
import {Routes,Route} from "react-router-dom";
import {About} from "./pages/About";
import {Home} from "./pages/Home"
import React from "react";
import {Navbar} from "./pages/Navbar";
import {Vans} from "./pages/Vans";
import {VanDetail} from "./pages/VanDetail.jsx";

function App() {
  return (
    <div className="max-w-full m-auto font-bold">
        <Navbar/>
        <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/home" element={<Home/>} />
            <Route path="/about" element={<About/>} />
            <Route path="/vans" element={<Vans/>} />
            <Route path="/vans/:id" element={<VanDetail/>} />

        </Routes>
    </div>
  )
}

export default App
